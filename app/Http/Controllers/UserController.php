<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

//require 'vendor/autoload.php';
use Intervention\Image\ImageManager;

class UserController extends Controller
{
    /**
     * Index view table with all users. All users get from ajax method get_index_users_ajax
     *
     * @return void
     */
    public function index()
    {
//        dd(storage_path().'\app\public\avatars');
//        $photo = User::find(31);
//        dd(Storage::url($photo->photo));

        // create an image manager instance with favored driver
        $manager = new ImageManager(array('driver' => 'imagick'));

// to finally create image instances
//        $image = $manager->make(Storage::url($photo->photo))->fit(300)->save(storage_path().'/app/public/'.$photo->photo, 8, 'jpg');
//        return $image->response('png');
//        dd($photo);
        return view('users.index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('users.create', ['positions' => Position::all()]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'             => 'required|min:2|max:256',
            'email'            => 'required|max:255|string|email|unique:App\Models\User,email',
            'phone'            => 'required|regex:/[0-9]/|not_regex:/[a-z]/|min:9|numeric',
            'position_id'      => 'required|numeric|exists:App\Models\Position,id',
            'salary'           => 'required|numeric',
            'head'             => 'required|numeric',
            'dateofemployment' => 'required',
            'photo'            => 'image|mimes:png,jpg|max:5000|dimensions:min_width=300,min_height=300'
        ]);

        DB::transaction(function () use ($request) {
            User::create([
                'name'             => $request->name,
                'email'            => $request->email,
                'phone'            => $request->phone,
                'photo'            => $request->photo !== null ? $request->file('photo')->store('avatars') : null,
                'position_id'      => $request->position_id,
                'salary'           => $request->salary,
                'admin_created_id' => $request->head,
                'admin_updated_id' => $request->head,
                'dateofemployment' => $request->dateofemployment,
                'password'         => Hash::make('qwerty12345') // TODO Что-то сделать с паролями по умолчанию
            ]);
        });

        return redirect()->route('users.index')->with('message', $request->name .' was created successfully');
    }

    /**
     * Edit view
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $user = User::find($id);
        if( $user->position_id !== null) {
            $user->position_id = $user->position()->first()['name'];
        }


        return view('users.edit', [
            'user' => $user,
            'positions' => Position::all()
        ]);
    }

    /**
     * Update data user
     *
     * @param [type] $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        // Validate Auth::user is PM?  TODO Через middlewhere сделать проверку кто пытается удалить (Просто для проработки middlewhere. Для логики это не нужно)
//        if (Auth::user()->position_id != 5)
//           return redirect()->back()->withErrors("You're not PM. Please. Send your PM");

        $request->validate([
            'name'         => 'required|min:2|max:256',
            'email'        => 'required|max:255|string|email|unique:App\Models\User,email,'.$id.',id',
            'phone'        => 'required|regex:/[0-9]/|not_regex:/[a-z]/|min:9|numeric',
            'position_id'  => 'required|numeric|exists:App\Models\Position,id',
            'salary'       => 'required|numeric',
            'photo'        => 'image|mimes:png,jpg|max:5000|dimensions:min_width=300,min_height=300'
        ]);

        DB::transaction(function () use ($id, $request) {
            $user = User::find($id);
            if ($request->photo !== null) // Validate. If is photo delete photo from storage
                Storage::delete($user->photo);

            $user->update([
                'name'        => $request->name,
                'email'       => $request->email,
                'phone'       => $request->phone,
                'position_id' => $request->position_id,
                'salary'      => $request->salary,
                'photo'       => $request->photo !== null ? $request->file('photo')->store('avatars') : $user->photo,
            ]);
        });
        return redirect()->back()->with('message', 'Сlient was change successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect()->route('users.index')->with('message', 'Deleted successfully');
    }

    /**
     * Get all users ajax
     *
     * @param Request $request
     * @return void
     */
    public function get_index_users_ajax(Request $request)
    {
        if ($request->ajax()) {
            return response()->json(['data' => User::all()->each(function ($item) {
                if($item->position_id !== null) {
                    $item->position_id = $item->position()->first()['name'];
                }
            })]);
        }
    }

    public function search_users_ajax(Request $request)
    {

        return response()->json(['data' => User::where('name', 'LIKE', "%{$request->letters}%")->get()]);
    }

}
