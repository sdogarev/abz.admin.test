<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PositionController extends Controller
{
    public function index()
    {
        return view('positions.index');
    }

    public function create()
    {
        return view('positions.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:255|string|unique:App\Models\Position'
        ]);

        DB::transaction(function () use ($request)
        {
            Position::create([
                'name' => $request->name
            ]);
        });

        return redirect()->route('positions.index')->with('message', $request->name .' was created successfully');
    }

    public function edit($id)
    {
        $position = Position::find($id);
        return view('positions.edit', ['position' => $position]);
    }

    /**
     * Update data user
     *
     * @param [type] $id
     * @param Request $request
     * @return void
     */
    public function update($id, Request $request)
    {
        // Validate Auth::user is PM?
//        if (Auth::user()->position_id != 5)
//           return redirect()->back()->withErrors("You're not PM. Please. Send your PM");
        $request->validate([
            'name' => 'required|min:2|max:256',
        ]);

        DB::transaction(function () use ($id, $request) {
            $user = Position::find($id);
            $user->update([
                'name' => $request->name,
            ]);
        });
        return redirect()->route('positions.index')->with('message', 'Position was change successfully');
    }

    public function destroy($id)
    {
        Position::destroy($id);

        return redirect()->route('positions.index')->with('message', 'Position was deleted successfully');
    }

    public function get_index_positions_ajax(Request $request)
    {
        if ($request->ajax()){
           return response()->json(['data' => Position::all()]);
        }
    }
}
