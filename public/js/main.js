$(document).ready(function() {

    const get_index_users_ajax = $('#get_index_users_ajax')
    if(0 < get_index_users_ajax.length) {
            $('#table').DataTable( {
                "ajax": get_index_users_ajax.val(),
                "async": true,
                "dataSrc": "data",
                "columns": [
                    {"data": "photo"},
                    {"data": "name"},
                    {"data": "position_id"},
                    {"data": "date_of_employment"},
                    {"data": "phone"},
                    {"data": "email"},
                    {"data": "salary"},
                    {"data": "id"},
                ],

                "order": [[1, 'asc']],
                "columnDefs": [
                    {
                        'targets': 0,
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            if (rowData.photo !== null) {
                                $(td).html(`<img src="storage/${rowData.photo}" style="width: 40px; height: 40px; border-radius: 50%; display: flex; margin: 0 auto">`);
                                // $(td).html(`<img src="${rowData.photo}" style="width: 40px; height: 40px; border-radius: 50%; display: flex; margin: 0 auto">`);
                            } else {
                                $(td).html(`<img src="https://via.placeholder.com/300x300.png/006611?text=Ai" style="width: 40px; height: 40px; background: silver; border-radius: 50%; display: flex; margin: 0 auto">`);
                            }

                        }
                    },
                    {
                        'targets': 2,
                        'createdCell': function (td, cellData, rowData, row, col) {
                            if (rowData.position_id !== null) {
                                $(td).html(`${rowData.position_id}`);
                            } else {
                                $(td).html('-');
                            }
                        }
                    },
                    {
                        'targets': 6,
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            if (rowData.salary !== null) {
                                $(td).html(`$${rowData.salary}`);
                            }
                        }
                    },
                    {
                        'targets': 7,
                        'createdCell':  function (td, cellData, rowData, row, col) {
                            let linkUpdate  = $('#url_update').val().replace('XXX', rowData.id)
                            let linkDestroy  = $('#url_destroy').val().replace('XXX', rowData.id)
                            $(td).html(`<span style="display: flex; justify-content: center;"><a href="${linkUpdate}"><i class="fas fa-pen" style="margin-right: 15px"></i></a><a href="${linkDestroy}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a></span>`);
                        }
                    },
                ]
            } );
    }

    /**
     * Positions
     */
    const get_index_positions_ajax = $('#get_index_positions_ajax')
    if(0 < get_index_positions_ajax.length) {
        $('#table').DataTable( {
            "ajax": get_index_positions_ajax.val(),
            "async": true,
            "dataSrc": "data",
            "columns": [
                {"data": "name"},
                {"data": "updated_at"},
                {"data": "id"},
            ],

            "order": [[1, 'asc']],
            "columnDefs": [
                {
                    'targets': 2,
                    'createdCell':  function (td, cellData, rowData, row, col) {
                        let linkUpdate  = $('#url_update').val().replace('XXX', rowData.id) // Route ссылка для обновления
                        let linkDestroy  = $('#url_destroy').val().replace('XXX', rowData.id) // Route ссылка для удаления
                        $(td).html(`<span style="display: flex; justify-content: center;"><a href="${linkUpdate}"><i class="fas fa-pen" style="margin-right: 15px"></i></a><a href="${linkDestroy}" onclick="return confirm('Are you sure?')"><i class="fas fa-trash"></i></a></span>`);
                    }
                },
            ]
        } );
    }

    /**
     * Alert close TODO Не работает. Возможно Session не успивает догрузить, из-за этого блока ещё нет на странице
     */
    const alertBlock = $('.alert')
    if (alertBlock.length) {
        setTimeout(() => {
            alertBlock.fadeOut(400, () => {
                alertBlock.remove()
            })
        }, 3000)
    }

    /**
     *  Form input words 0 for 256
     */

    const letter_counter = $('#letter_counter')
    if(letter_counter.length) {  // 0 < letter_counter.length?
        const inputName = $('#exampleInputName1')[0],
            max_letter_counter = +$('#max_letter_counter')[0].innerHTML,
            min_letter_counter = 2,
            box_letter_counter = $('#box_letter_counter')

        const drawUsersAjax = (val) => {
            const tests = $('#tests')[0].value

            $.ajax({
                method : 'POST',
                url : '/search_users_ajax',
                data : {letters : val, _token: tests,}
            }).done( data => {
                $('#drop_down_users li').remove() // delete all li after draw all users
                for (let i = 0; i<= data.data.length; i++) {
                    $('#drop_down_users').append(`<li class="drop_down_users_item" onclick="$('#exampleInputName1')[0].value = this.innerHTML; $('#drop_down_users li').remove()" style="list-style-type: none; padding: 15px 20px; cursor: pointer;">${data.data[i]['name']}</li>`)
                }
            })
        }

        letter_counter[0].innerHTML = '0'

        inputName.addEventListener('input', (e) => {
            let val = e.target.value
            letter_counter[0].innerHTML = val.length // draw 0 in page 0 / 256

            if (e.target.value.length >= max_letter_counter || e.target.value.length < min_letter_counter) {
                box_letter_counter.addClass('text-danger')
                $('#drop_down_users li').remove()
            } else {
                box_letter_counter.removeClass('text-danger')
                drawUsersAjax(val)
            }
        })
    }

})

