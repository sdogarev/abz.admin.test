<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {
    //Employees
    Route::get('/', [App\Http\Controllers\IndexController::class, 'index'])->name('index');
    Route::get('/employees', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
    Route::get('/employees/{id}/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
    Route::post('/employees/{id}/update', [App\Http\Controllers\UserController::class, 'update'])->name('users.update');
    Route::get('/employees/create', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
    Route::post('/employees/store', [App\Http\Controllers\UserController::class, 'store'])->name('users.store');
    Route::get('/employees/{id}/destroy', [\App\Http\Controllers\UserController::class, 'destroy'])->name('users.destroy');

    //Positions
    Route::get('/positions', [App\Http\Controllers\PositionController::class, 'index'])->name('positions.index');
    Route::get('/positions/create', [App\Http\Controllers\PositionController::class, 'create'])->name('position.create');
    Route::post('/positions/store', [App\Http\Controllers\PositionController::class, 'store'])->name('position.store');
    Route::get('/positions/{id}/edit', [App\Http\Controllers\PositionController::class, 'edit'])->name('position.edit');
    Route::post('/positions/{id}/update', [App\Http\Controllers\PositionController::class, 'update'])->name('position.update');
    Route::get('/positions/{id}/destroy', [App\Http\Controllers\PositionController::class, 'destroy'])->name('position.destroy');

    // AJAX
    Route::get('/get_index_users_ajax', [App\Http\Controllers\UserController::class, 'get_index_users_ajax'])->name('users.ajax.index');
    Route::get('/get_index_position_ajax', [App\Http\Controllers\PositionController::class, 'get_index_positions_ajax'])->name('positions.ajax.index');
    Route::post('/search_users_ajax', [App\Http\Controllers\UserController::class, 'search_users_ajax'])->name('users.ajax.search');

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});





Auth::routes();


