<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Carbon\Carbon;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('position_id')->nullable();
            $table->string('date_of_employment')->default(Carbon::now());
            $table->string('phone', 22)->unique()->nullable();
            $table->string('email')->unique();
            $table->double('salary', 8, 2)->default(0.00);
            $table->text('photo')->nullable();
            // $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('admin_created_id')->nullable();
            $table->integer('admin_updated_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
