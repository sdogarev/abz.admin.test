<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'               => $this->faker->name,
            'position_id'        => $this->faker->numberBetween(1, 5),
            'date_of_employment' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'phone'              => $this->faker->unique()->e164PhoneNumber,
            'email'              => $this->faker->unique()->safeEmail,
            'salary'             => $this->faker->numberBetween($min = 1000, $max = 9000),
            'photo'              => null,
            'password'           => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token'     => Str::random(10),
        ];
    }
}
