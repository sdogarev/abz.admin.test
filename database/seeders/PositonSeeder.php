<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PositonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $positions = \App\Models\Position::factory()->count(5)->create();
    }
}
