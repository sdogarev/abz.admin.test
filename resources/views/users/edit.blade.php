@extends('adminlte::page')

@section('title', 'Employees edit')

@section('content_header')
    <h1>Employees edit</h1>
@stop

@section('content')

@if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger" role="alert">
        {{ $error }}
    </div>
    @endforeach
@endif

@if(session('message'))
    <div class="alert alert-success" role="alert">
        {{ session('message') }}
    </div>
@endif

<form method="POST" enctype="multipart/form-data" action="{{ route('users.update', ['id' => $user->id]) }}">
@csrf
    <div class="card-body">
        <div class="form-group">
            <div class="widget-user-image">
                <img class="img-circle" style="height: 100px; width: 100px;" src="{{ $user->photo ? Storage::url($user->photo) : 'https://via.placeholder.com/300x300.png/006611?text=Ai' }}" alt="User Avatar">
            </div>
            <label for="exampleInputPhoto">Photo</label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="photo" id="exampleInputPhoto">
                    <label class="custom-file-label" for="exampleInputPhoto">Choose file</label>
                </div>
                {{--                <div class="input-group-append">--}}
                {{--                    <span class="input-group-text">Upload</span>--}}
                {{--                </div>--}}
            </div>
        </div>
        <div class="form-group">
            <label for="exampleInputName1">Name</label>
            <input type="text" class="form-control" value="{{ $user->name }}" name="name" id="exampleInputName1" placeholder="Name">
            <div id="box_letter_counter" style="text-align: right"><span id="letter_counter"></span> / <span id="max_letter_counter">256</span></div>
            <ul id="drop_down_users" style="position: absolute; width: 83%; background: white; border-radius: .25rem; padding: 0">

            </ul>
        </div>
        <div class="form-group">
            <label for="exampleInputEmployment1">Date of employment</label>
            <input type="text" class="form-control" value="{{ $user->date_of_employment }}" name="date_of_employment" id="exampleInputEmployment1" placeholder="Employment">
        </div>
        <div class="form-group">
            <label for="exampleInputPosition1">Position</label>
            <select class="custom-select" name="position_id">
                @foreach($positions as $position)
                    <option value="{{ $position->id }}" {{ $position->name == $user->position_id ? 'selected' : '' }}>{{ $position->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputSalary1">Salary</label>
            <input type="text" class="form-control" value="{{ $user->salary }}" name="salary" id="exampleInputSalary1" placeholder="Salary">
        </div>
        <div class="form-group">
            <label for="exampleInputPhone1">Phone</label>
            <input type="text" class="form-control" value="{{ $user->phone }}" name="phone" id="exampleInputPhone1" placeholder="Phone">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" name="email" value="{{ $user->email }}" placeholder="Enter email">
        </div>
        <!-- <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div> -->

        <input type="text" id="tests" value="{{ csrf_token() }}" hidden>
        <div class="form-group">
            <div class="row">
                <div class="col-md-4">
                    <p><strong>Created at:</strong> {{ $user->created_at }}</p>
                    <p><strong>Updated at:</strong> {{ $user->updated_at }}</p>
                </div>
                <div class="col-md-4">
                    <p><strong>Admin created id:</strong> {{ $user->admin_created_id ? $user->admin_created_id : '-' }}</p>
                    <p><strong>Admin updated id:</strong> {{ $user->admin_updated_id ? $user->admin_updated_id : '-' }}</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script src="{{ asset('/js/main.js') }}"></script>
@stop
