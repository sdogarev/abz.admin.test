@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <div class="d-flex justify-content-between">
        <h1>Employees</h1>
        <a href="{{ route('users.create') }}" class="btn btn-info float-right">Add employ</a>
    </div>
@stop

@section('content')
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    @if(session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif
    <input type="hidden" id="get_index_users_ajax" value="{{ route('users.ajax.index') }}">
    <input type="hidden" id="url_update" value="{{ route('users.edit', ['id' => 'XXX']) }}">
    <input type="hidden" id="url_destroy" value="{{ route('users.destroy', ['id' => 'XXX']) }}">
    <table id="table" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
        <thead>
            <tr>
                <th>Photo</th>
                <th>Name</th>
                <th>Position</th>
                <th>Date of employment</th>
                <th>Phone number</th>
                <th>Email</th>
                <th>Salary</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script src="{{ asset('/js/main.js') }}"></script>
@stop
