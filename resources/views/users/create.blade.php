@extends('adminlte::page')

@section('title', 'Employees create')

@section('content_header')
    <h1>Employees create</h1>
@stop

@section('content')

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    @if(session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif

    <form method="POST" enctype="multipart/form-data" action="{{ route('users.store') }}">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <div class="widget-user-image">
                    <img class="img-circle" style="height: 100px; width: 100px;" src="https://via.placeholder.com/300x300.png/001155?text=Ai" alt="User Avatar">
                </div>
                <label for="exampleInputPhoto">Photo</label>
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="photo" id="exampleInputPhoto">
                        <label class="custom-file-label" for="exampleInputPhoto">Choose file</label>
                    </div>
                    {{--                <div class="input-group-append">--}}
                    {{--                    <span class="input-group-text">Upload</span>--}}
                    {{--                </div>--}}
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputName1">Name</label>
                <input type="text" class="form-control" name="name" id="exampleInputName1" placeholder="Name">
                <div id="box_letter_counter" style="text-align: right"><span id="letter_counter"></span> / <span id="max_letter_counter">256</span></div>
            </div>
            <div class="form-group">
                <label for="exampleInputPosition1">Position</label>
                <select class="custom-select" name="position_id">
                    @foreach($positions as $position)
                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputSalary1">Salary</label>
                <input type="text" class="form-control" name="salary" id="exampleInputSalary1" placeholder="Salary">
            </div>
            <div class="form-group">
                <label for="exampleInputPhone1">Phone</label>
                <input type="text" class="form-control"name="phone" id="exampleInputPhone1" placeholder="Phone">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Head</label>
                <input type="text" class="form-control" value="{{ Auth::user()->id }}" id="exampleInputEmail1" name="head" placeholder="Enter email" hidden>
                <input type="text" class="form-control" value="{{ Auth::user()->name }}" id="exampleInputEmail1" placeholder="Enter email" disabled>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Date of employment</label>
                <input type="date" class="form-control" id="exampleInputEmail1" name="dateofemployment" placeholder="Enter email" >
            </div>
            <!-- <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div> -->
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{ asset('/js/main.js') }}"></script>
@stop
