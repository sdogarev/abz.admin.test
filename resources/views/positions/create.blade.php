@extends('adminlte::page')

@section('title', 'Position create')

@section('content_header')
    <h1>Position create</h1>
@stop

@section('content')

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif

    @if(session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif

    <form method="POST" action="{{ route('position.store') }}">
        @csrf
        <div class="card-body">
            <div class="card card-info w-50">
                <div class="card-header">
                    <h3 class="card-title">Position create</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="inputName3" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" id="exampleInputName1" placeholder="Name">
                                <div id="box_letter_counter" style="text-align: right"><span id="letter_counter"></span> / <span id="max_letter_counter">256</span></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer d-flex justify-content-end">
                        <a href="{{ url()->previous() }}" class="btn btn-default mr-3">Cancel</a>
                        <button type="submit" class="btn btn-info float-right">Save</button>
                    </div>
                    <!-- /.card-footer -->
                </form>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{ asset('/js/main.js') }}"></script>
@stop
